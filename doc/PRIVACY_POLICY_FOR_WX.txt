Title: Privacy Policy
For: Android “wX” app and iOS “wXL23” app
Comments/questions: please email app creator joshua.tee@gmail.com
ID: joshuatee.wx
Last Update: 2024-09-22
----------------------
These applications DO NOT collect, transmit, distribute or sell your data.
These applications and its developer take your privacy very seriously.
It DOES NOT use third-party analytics or advertising frameworks.
----------------------

